var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');


describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () => {
      it('Status 200', () => {
          expect(Bicicleta.allBicis.length).toBe(0);
          var a = new Bicicleta(1, 'rojo','urbana',[10.4967289,-66.8814518]);
          Bicicleta.add(a);
          request.get('http://localhost:5000/api/bicicletas', function(error,response,body){
                expect(response.statusCode).toBe(200);
          });
      });
    });
    
    describe('POST BICICLETAS /create', () => {
      it('STATUS 200', (done) => {
         var headers = {'content-type':'application/json'};
         var aBici = '{"id":10,"color":"rojo","modelo":"urbana","lat":10,"lng":-66}';
         request.post({
           headers: headers,
           url: 'http://localhost:5000/api/bicicletas/create',
           body: aBici
         }, function(error, response, body){
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(10).color).toBe('rojo');
            done(); 
         });
     });
    });
    
    describe('ELIMINAR BICICLETAS /delete', () => {
      it('STATUS 200', (done) => {
         var headers = {'content-type':'application/json'};
         var aBici = '{"id":10,"color":"rojo","modelo":"urbana","lat":10,"lng":-66}';
         request.post({
           headers: headers,
           url: 'http://localhost:5000/api/bicicletas/delete',
           body: aBici
         }, function(error, response, body){
            //expect(response.statusCode).toBe(200);
            var eliminarById = Bicicleta.removeById(10);
            //expect(Bicicleta.allBicis.length).toBe(0);
            
            done(); 
         });
     });
    });

    describe('Actualizar BICICLETAS /Update', () => {
      it('Status 200', () => {
        var headers = {'content-type':'application/json'};
        var aBici = '{"id":10,"color":"rojo","modelo":"urbana","lat":10,"lng":-66}';
        request.post({
          headers: headers,
          url: 'http://localhost:5000/api/bicicletas/10/update',
          body: aBici
        }, function(error, response, body){
          var valores =Bicicleta.allBicis.length;
          expect(valores).toBe(0);
          var a = new Bicicleta(1, 'rojo','urbana',[10.4967289,-66.8814518]);
          Bicicleta.add(a); 
          //expect(response.statusCode).toBe(200);
           var Bici = Bicicleta.findById(1);
           Bici.color = "azul";
           Bici.modelo = "montaña";
           Bici.ubicacion = [10,-66];
           expect(Bici.color).toBe('azul');


           //done(); 
        });
      });
    });
    
 
  });

