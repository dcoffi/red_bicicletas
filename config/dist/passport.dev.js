"use strict";

var passport = require('passport');

var Usuario = require('../models/usuario');

var LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(function (email, password, done) {
  Usuario.findOne({
    email: email
  }, function (err, usuario) {
    if (err) return done(null);
    if (!usuario) return done(null, false, {
      message: 'Email no existe o esta mal escrito.'
    });
    if (!usuario.validPassword(password)) return done(null, false, {
      message: 'El password ingresado no es valido.'
    });
    return done(null, usuario);
  });
}));
passport.serializeUser(function (user, cb) {
  cb(null, user.id);
});
passport.deserializeUser(function (id, cb) {
  Usuario.findById(id, function (err, usuario) {
    cb(err, usuario);
  });
});
module.exports = passport;