//require('newrelic');
require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const jwt = require('jsonwebtoken');
var indexRouter = require('./routes/index');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var authApiRouter = require('./routes/api/auth');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');

let store;

if (process.env.NODE_ENV === 'development') {
  store = session.MemoryStore();

} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error', function (err) {
    assert.ifError(err);
    assert.ok(false);
  });
}

var app = express();
app.set('secretKey', 'jwt_pwd!!prueba');
app.use(session({
  cookie: { maxAge: 240 * 60 *60 * 1000},
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicis_!!!***!".!".!".!".!".!".123123'
}));
var mongoose = require('mongoose');
const Usuario = require('./models/usuario');
const Token = require('./models/token');
var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, {useNewUrlParser: true,useUnifiedTopology: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error',console.error.bind(console,'MongoDB connection error'));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login', function (req, res) {
	res.render('session/login');
});

app.post('/login', function (req, res, next) {
	//passport
	passport.authenticate('local', function (err, usuario, info) {
		if (err) return next(err);
		if (!usuario) return res.render('session/login', { info });
		req.logIn(usuario, function (err) {
			if (err) return next(err);
			return res.redirect('/');
		});
	})(req, res, next);
});

app.get('/logout', function (req, res) {
	/*req.logout();
	res.redirect('/');*/
	req.session.destroy(function(e){
		req.logout();
		res.redirect("/login");
		});
});

app.get('/forgotPassword', function(req, res){
	return res.render('session/forgotPassword')
  });
  
  app.post('/forgotPassword', function(req, res){
	Usuario.findOne({email: req.body.email}, function(err, usuario){
	  if(err){return console.log(err)};
	  if(!usuario) {
		return res.render('session/forgotPassword', {
		  info: {
			message: 'El email ingresado no existe'
		  }
		}
	  )
	};
	//si existe el usuario, entonces enviar correo con nuevo token para modificar
	//la contraseña
	usuario.resetPassword(function(err){
	  if(err) {return next(err)};
	});
	//Cuando se envía el mail, entonces renderear el get a resetPassword
	res.render('session/forgotPasswordMessage', );
	});
  });
  
  //Cargar el get de reset pasword para que las personas ingresen con el token enviado
  app.get('/resetPassword/:token', function(req, res, next){
	Token.findOne({token: req.params.token}, function(err, token){
	  //Si no existe un usuario que tenga asignado ese token
	  //enviar un mensaje diciendo que no existe el usuario
	  if (!token) {
		return res.status(400).send({
		type: 'not-verified',
		msg: 'No existe un usuario asociado a este token. Verifique que no haya expirado su token'
		}); 
	  };
	  //Si existe el usuario, entonces sacar el usuario que tiene ese token
	  Usuario.findById(token._userId, function(err, usuario) {
		
		if(err){
		  return next(err);
		};
		//Si no encuentro un usuario con el token dado, entonces enviar que no existe el usuario
		if(!usuario){
		  return res.status(400).send(
			{
			  msg: 'No existe un usuario asociado a este token.'
			});
		};
		//Si el usuario, existe y el token es correcto, Entonces envío el usuario a resetPassword
		res.render('session/resetPassword', {
		  errors: {},
		  usuario: usuario
		});
	  });
	});
  });
  //Post to resetPassword Need a body params Email & Password
  app.post('/resetPassword', function(req, res, next){
	//las contraseñas son diferentes?
	if(req.body.password != req.body.confirm_password){
	  //Si las contraseñas son diferentes, entonces enviar un error
	  res.render('session/resetPassword', {
		errors: {password: 'No Coinciden las contraseñas ingresadas'},
		usuario: new Usuario({email: req.body.email})
	  });
	  return
	};
	//Si las claves son iguales, entonces voy con el mail, busco el usuario, y actualizo la contraseña
	Usuario.findOne({email: req.body.email}, function(err, usuario){
	  //si hay un error, pasa al siguiente
	  usuario.password = req.body.password;
	  usuario.save(function(err){
		if (err) {
		  res.render('session/resetPassword', 
			{ 
			  errors:err.errors, 
			  usuario: new Usuario({email: req.body.email})
			}
		  );
		} else {
		  res.redirect('/login');
		};
	  });
	  //--Finish callback function FindOne
	});
  });
  

//sapp.use('/',loggedIn, indexRouter);
app.use('/api/bicicletas',validarUsuario, bicicletasAPIRouter);
app.use('/', indexRouter);
app.use('/usuarios',loggedIn,usuariosRouter);
//app.use('/usuarios',usuariosRouter);
app.use('/token',tokenRouter);
app.use('/users', usersRouter);
app.use('/bicicletas',loggedIn,bicicletasRouter);
//app.use('/bicicletas',bicicletasRouter);
app.use('/api/auth',authApiRouter);
app.use('/api/bicicletas',validarUsuario, bicicletasAPIRouter);
//app.use('/api/bicicletas', bicicletasAPIRouter);
app.use('/api/usuarios',usuariosAPIRouter);
app.use('/privacy_policy',function(req,res){
	res.sendFile('public/policy_privacy.html');
});
app.use('/google378389ad53cbd8a4.html',function(req,res){
	res.sendFile('public/google378389ad53cbd8a4.html');
});

app.get('/auth/google',
  passport.authenticate('google', {
    scope: [
      'https://www.googleapis.com/auth/plus.login',
      'https://www.googleapis.com/auth/plus.profile.emails.read',
      'profile',
      'email'
    ] 
  })
);

app.get('/auth/google/callback',
  passport.authenticate('google', {
    successRedirect: '/',
	failureRedirect: '/error'

  })
);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next) {
  if(req.user){
    next();
  }else{
    console.log('Usuario sin loguearse.');
    res.redirect('/login');
  }
};
function validarUsuario(req, res, next) {
	jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
		if (err) {
			res.json({ status: "error", message: err.message, data: null });
		} else {
			req.body._userId = decoded.id;

			console.log('jwt verify ' + decoded);

			next();

		}
	});
}
module.exports = app;
