var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;

var reservaSchema = new Schema({
    desde: Date,
    hasta: Date,
    //aquí se le pasa el nombre del modelo en este caso Bicicleta
    bicicleta:{ type: mongoose.Schema.Types.ObjectId, ref:'Bicicleta' },
    //aquí se le pasa el nombre del modelo en este caso Usuario
    usuario:{ type: mongoose.Schema.Types.ObjectId, ref:'Usuario'},
});

reservaSchema.methods.diasDeReserva = function(){
  return moment(this.hasta).diff(moment(this.desde),'days') + 1;
};

module.exports = mongoose.model('Reserva', reservaSchema);